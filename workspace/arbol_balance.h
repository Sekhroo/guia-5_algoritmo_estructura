#ifndef ARBOL_BALANCE_H
#define ARBOL_BALANCE_H

#include <iostream>
using namespace std;

typedef struct _Nodo {
    string dato;
    int nivel;
    int FE;
    struct _Nodo *next_left;
    struct _Nodo *next_right;
} Nodo;

class arbol_balance {

    private:
        Nodo *raiz_arbol = NULL;

    public:
        arbol_balance();

        /* Funciones de soporte para balancear el árbol */
        int nivel_nodo(Nodo *arbol);
        int valor_max(int a, int b);
        int get_balance(Nodo *arbol);


        /* Funciones para restructurar el árbol en caso de desbalanceo*/
        Nodo* right_rotate(Nodo *y);
        Nodo* left_rotate(Nodo *x);


        /* Funciones para crear e insertar nuevos nodos al árbol */
        Nodo* new_nodo(string dato);
        Nodo* insert_nodo(Nodo *arbol, string dato);

        /* Funciones para buscar o cambiar un dato especifico */
        Nodo* search_nodo(Nodo *arbol, string dato);
        Nodo* change_nodo(Nodo *arbol, string dato);


        /* Funciones para eliminar un dato especifico */
        Nodo* min_left_nodo(Nodo *arbol);
        Nodo* delete_nodo(Nodo *arbol, string dato);


        /* Funciones para mostrar los datos almacenados en el árbol avl */
        void mostrar_arbol(Nodo *arbol);
        void mostrar_arbol_preorden(Nodo *arbol);
        void mostrar_arbol_inorden(Nodo *arbol);
        void mostrar_arbol_postorden(Nodo *arbol);


        /* Función para visualizar un grafo */
        Nodo* lectura_txt(Nodo *arbol, string dato);

        /* Función para visualizar un grafo */
        void crear_grafo(Nodo *arbol);
};
#endif