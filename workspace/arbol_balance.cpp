#include <iostream>
#include <fstream>
#include <string>
#include <string.h> 
using namespace std;

#include "arbol_balance.h"
#include "grafo.h"

arbol_balance::arbol_balance(){}

/* Función para verificar el nivel de cada sub_nodo */
int arbol_balance::nivel_nodo(Nodo *arbol) {
    // Si el nodo esta vacío regresa 0, ya que no hay nivel en vacío
    if (arbol == NULL)
        return 0;

    // Si no esta vacío regresa el nivel del nodo
    return arbol->nivel;
}

/* Función para conseguir el valor mayor entre a y b */
int arbol_balance::valor_max(int a, int b) {
    return (a > b)? a : b;
}


/* Diagrama de rotación:

       y                                x
      / \       Right Rotation         / \
     x   T3     - - - - - - - >       T1  y 
    / \         < - - - - - - -          / \
   T1  T2       Left Rotation           T2  T3 

*/

/* Función para hacer una rotacion hacia la derecha*/
Nodo* arbol_balance::right_rotate(Nodo *y) {
    // Nodos temporales de rotación
    Nodo *x = y->next_left;
    Nodo *T2 = x->next_right;
 
    // La ratoración per se
    x->next_right = y;
    y->next_left = T2;

    // Actualizar niveles
    y->nivel = valor_max(nivel_nodo(y->next_left),nivel_nodo(y->next_right)) + 1;
    x->nivel = valor_max(nivel_nodo(x->next_left),nivel_nodo(x->next_right)) + 1;

    // Regresar el nuevo orden de nodos
    return x;
}

/* Función para hacer una rotacion hacia la izquierda*/
Nodo* arbol_balance::left_rotate(Nodo *x) {
    // Nodos temporales de rotación
    Nodo *y = x->next_right; 
    Nodo *T2 = y->next_left;
 
    // La ratoración per se
    y->next_left = x;
    x->next_right = T2;
 
    // Actualizar niveles
    x->nivel = valor_max(nivel_nodo(x->next_left),nivel_nodo(x->next_right)) + 1;
    y->nivel = valor_max(nivel_nodo(y->next_left),nivel_nodo(y->next_right)) + 1;
 
    // Regresar el nuevo orden de nodos
    return y;
}

/* Función para registrar la diferencia de nivel en la descendencia */
int arbol_balance::get_balance(Nodo *arbol) {
    // Si el arbol esta vacío regresar como diferencia de nivel 0
    if (arbol == NULL)
        return 0;
    
    // Sino regresar la diferencia de nivel de la descendencia
    return nivel_nodo(arbol->next_left) - nivel_nodo(arbol->next_right);
}


/* Función para crear un nuevo nodo */
Nodo* arbol_balance::new_nodo(string dato) {
    Nodo* arbol = new Nodo();
    arbol->dato = dato;
    arbol->next_left = NULL;
    arbol->next_right = NULL;
    arbol->FE = 0; // Empieza como hoja
    arbol->nivel = 1; //EL nuevo nodo inicia con nivel 1

    
    return(arbol);
}

/* Función para añadir el nuevo nodo al árbol */
Nodo* arbol_balance::insert_nodo(Nodo *arbol, string dato) {
    // Si el nodo esta vacío, se crea un nodo nuevo
    if (arbol == NULL)
        return(new_nodo(dato));
    
    
    // Se busca donde insertar el nodo con recursividad
    if (dato < arbol->dato) { // Izquierda
        arbol->next_left = insert_nodo(arbol->next_left, dato);
    }else if (dato > arbol->dato) { // Derecha
        arbol->next_right = insert_nodo(arbol->next_right, dato);

    // Los árboles no permiten dos valores iguales
    }else {
        cout << "La proteína ya se encuentra en el árbol. " << endl << endl;
        return arbol;
    }


    // Se evalúa el nivel del nodo
    arbol->nivel = 1 + valor_max(nivel_nodo(arbol->next_left), nivel_nodo(arbol->next_right));
 
    // Verificar si este nodo desequilibro el árbol
    arbol->FE = get_balance(arbol);
 
    // Se evalúan los 4 casos
    // Si el nivel supera a el 2 de diferencia entonce ocurre una rotación
 
    // Rotación II
    if (arbol->FE > 1 && dato < arbol->next_left->dato) {
        return right_rotate(arbol);
    }
    
    // Rotación DD
    if (arbol->FE < -1 && dato > arbol->next_right->dato) {
        return left_rotate(arbol);
    }

    // Rotación ID
    if (arbol->FE > 1 && dato > arbol->next_left->dato) {
        arbol->next_left = left_rotate(arbol->next_left);
        return right_rotate(arbol);
    }
 
    // Rotación DI
    if (arbol->FE < -1 && dato < arbol->next_right->dato) {
        arbol->next_right = right_rotate(arbol->next_right);
        return left_rotate(arbol);
    }

    return arbol;
}


/* Función para buscar un nodo en el árbol */
Nodo* arbol_balance::search_nodo(Nodo *arbol, string dato) {
    // Valor no encontrado o árbol vacío
    if (arbol == NULL){
        cout << "El árbol no contiene la proteína seleccionada. " << endl << endl;
        return arbol;
    }

    // Recorrer el árbol mediante recursividad
    if (arbol->dato > dato) {
        search_nodo(arbol->next_left, dato); // Recursiva nodo izquierdo

    }else if (arbol->dato < dato) {
        search_nodo(arbol->next_right, dato); // Recursiva nodo derecho

    // Valor encontrado
    }else if (arbol->dato == dato){
        cout << "Proteína encontrada. " << endl;
        
        return arbol;
    }

    return arbol;
}

/* Función para cambiar el dato de un nodo */
Nodo* arbol_balance::change_nodo(Nodo *arbol, string dato) {
    // Eliminar el termino indicado
    arbol = delete_nodo(arbol, dato);
    
    system("clear"); // Limpiar terminal
    
    cout << "Ingrese la nueva proteína: "; // Preguntar por cual valor cambiar
    cin >> dato;

    system("clear"); // Limpiar terminal

    // Insertar nuevo valor
    arbol = insert_nodo(arbol, dato);

    return arbol;
}


/* Función para registrar el nodo mínimo izquierda */
Nodo* arbol_balance::min_left_nodo(Nodo *arbol) {
    Nodo *indice = arbol; // Intanciar un nodo temporal indice
 
    while (indice->next_left != NULL) // Recorrer
        indice = indice->next_left; 
 
    return indice; // Retornar el minimo nodo izquierdo
}

/* Función para eliminar un nodo específico */
Nodo* arbol_balance::delete_nodo(Nodo *arbol, string dato) {
    
    // Si el árbol esta vacío
    if (arbol == NULL) {
        cout << "Proteína no encontrada en el árbol. " << endl << endl;
        return NULL; // Volver
    
    // Recorrer el árbol en busca del nodo
    }else if (dato < arbol->dato) {
        arbol->next_left = delete_nodo(arbol->next_left, dato); // Iterar por la izquierda
    
    }else if (dato > arbol->dato) {
        arbol->next_right = delete_nodo(arbol->next_right, dato); // Iterar por la derecha
    
    }else { 
        // En en caso de que el nodo tenga 1 o 0 descendientes 
        if (arbol->next_left == NULL) {
            Nodo *temp = arbol->next_right;
            delete arbol;
            return temp;

        }else if (arbol->next_right == NULL) {
            Nodo *temp = arbol->next_left;
            delete arbol;
            return temp;

        // En el caso de que el nodo tenga 2 descendientes
        }else {
            
            // Buscar el mínimo izquierda del nodo derecho
            Nodo *temp = min_left_nodo(arbol->next_right);

            // Copiar el contenido del descendiente 
            arbol->dato = temp->dato;

            // Eliminar descendiente
            arbol->next_right = delete_nodo(arbol->next_right, temp->dato);
        }
    }

    cout << "La proteína [" << dato << "] ha sido eliminada del árbol. " << endl << endl;

    // Verificar que se mantega el orden del árbol balanceado
    int equival = get_balance(arbol);

    // Caso de rotación DD
    if (equival == 2 && get_balance(arbol->next_left) >= 0)
        return right_rotate(arbol);
    
    // Caso de rotación ID
    else if (equival == 2 && get_balance(arbol->next_left) == -1) {
        arbol->next_left = left_rotate(arbol->next_left);
        return right_rotate(arbol);
    }
    // Caso de rotación II	
    else if (equival == -2 && get_balance(arbol->next_right) <= -0)
        return left_rotate(arbol);
    
    // Caso de rotación DI
    else if (equival == -2 && get_balance(arbol->next_right) == 1) {
        arbol->next_right = right_rotate(arbol->next_right);
        return left_rotate(arbol);
    }

    return arbol;
}


/* Función para mostrar todos los nodos del arbol de todas las formas: preorden, inorden y postorden */
void arbol_balance::mostrar_arbol(Nodo *arbol) {

    if (arbol != NULL) {
        //Mostrar en preorden
        cout << "Preorden: ";
        mostrar_arbol_preorden(arbol);
        cout << endl;


        //Mostrar en inorden
        cout << "Inorden: ";
        mostrar_arbol_inorden(arbol);
        cout << endl;


        //Mostrar en postorden
        cout << "Postorden: ";
        mostrar_arbol_postorden(arbol);
        cout << endl << endl;
    
    }else {
        cout << "El árbol se encuentra vacío. " << endl << endl;
    }
}

/* Función para mostrar todos los nodos del arbol en preorden */
void arbol_balance::mostrar_arbol_preorden(Nodo *arbol) {
    if (arbol != NULL) {
        cout << "[" << arbol->dato << "]  ";
        mostrar_arbol_preorden(arbol->next_left);
        mostrar_arbol_preorden(arbol->next_right);
    }
}

/* Función para mostrar todos los nodos del arbol en inorden */
void arbol_balance::mostrar_arbol_inorden(Nodo *arbol) {
    if (arbol != NULL) {
        mostrar_arbol_inorden(arbol->next_left);
        cout << "[" << arbol->dato << "]  ";
        mostrar_arbol_inorden(arbol->next_right);
    }
}

/* Función para mostrar todos los nodos del arbol en postorden */
void arbol_balance::mostrar_arbol_postorden(Nodo *arbol) {
    if (arbol != NULL) {
        mostrar_arbol_postorden(arbol->next_left);
        mostrar_arbol_postorden(arbol->next_right);
        cout << "[" << arbol->dato << "]  ";
    }
}


/* Función para leer un archivo.txt indicado por el usuario*/
Nodo* arbol_balance::lectura_txt(Nodo* arbol, string dato){
    
    ifstream archivo; // Instanciar lectura de archivo

    archivo.open(dato,ios::in); // Abrir el archivo con nombre = dato

    if(archivo.is_open()) { // Mientras este abierto
        string linea;
        while(getline(archivo,linea)) { // Recorrera el archivo
            arbol = insert_nodo(arbol,linea); // almacenando cada linea en el árbol
            
            //cout << linea << endl; // Imprimir archivo para verificar su apertura
        }
        archivo.close(); // Cerrar archivo
        return arbol;

    }else { // Si no se encuentra el archivo con nombre = dato
        cout << "No se encontro el archivo.txt" << endl << endl;
        return arbol;
    }
    return arbol;
}

/* Función para crear la imagen con el grafo junto al archivo .txt*/
void arbol_balance::crear_grafo(Nodo *arbol) {
    grafo *generar_grafo = new grafo(arbol);
}