#include <iostream>
#include<fstream>

using namespace std;

#include "grafo.h"
#include "arbol_balance.h"

grafo::grafo(Nodo *nodo){
    ofstream fp;
            
    /* abre archivo */
    fp.open ("grafo.txt");

    fp << "digraph G {" << endl;
    fp << "node [style=filled fillcolor=yellow];" << endl;

    fp << "nullraiz [shape=point];" << endl; 
    fp << "nullraiz->" << '"' << nodo->dato << '"' << "[label=" << nodo->nivel << "];" << endl;
                
    recorrer_arbol(nodo, fp);

    fp << "}" << endl;

    /* cierra archivo */
    fp.close();
                    
    /* genera el grafo */
    system("dot -Tpng -ografo.png grafo.txt &");
            
    /* visualiza el grafo */
    system("eog grafo.png &");
}


void grafo::recorrer_arbol(Nodo *arbol, ofstream &archivo) {  
    if (arbol != NULL) {
        if (arbol->next_left != NULL) { 
            archivo << "\n" << '"' << arbol->dato << '"' << "->" << '"' << arbol->next_left->dato << '"' << " [label=" << arbol->next_left->nivel << "];";

        } else {
            archivo << "\n" << '"' << arbol->dato << "i" << '"' << " [shape=point];";
            archivo << "\n" << '"' << arbol->dato << '"' << "->" << '"' << arbol->dato << "i" << '"';
        }
                    
        if (arbol->next_right != NULL) {
            archivo << "\n" << '"' << arbol->dato << '"' << "->" << '"' << arbol->next_right->dato << '"' << " [label=" << arbol->next_right->nivel << "];";

        } else {
            archivo << "\n" << '"' << arbol->dato << "d" << '"' << " [shape=point];";
            archivo << "\n" << '"' << arbol->dato << '"' << "->" << '"' << arbol->dato << "d" << '"';
        }

        recorrer_arbol(arbol->next_left, archivo);
        recorrer_arbol(arbol->next_right, archivo); 
    }
}