#include<iostream>
using namespace std;

#include "arbol_balance.h"


/* Función para limpiar la terminal */
void clear() {
    cout << "\x1B[2J\x1B[H";
}

/* Función para el menu principal */
string menu_principal (string opt) {
    cout << "============= Menu =============" << endl;
    cout << "Agregar una proteína _______ [1]" << endl;
    cout << "Buscar una proteína  _______ [2]" << endl;
    cout << "Cambiar una proteína _______ [3]" << endl;
    cout << "Eliminar una proteína  _____ [4]" << endl;
    cout << "Mostrar proteínas __________ [5]" << endl;
    cout << "Leer archivo externo _______ [6]" << endl;
    cout << "Visualizar en grafo ________ [7]" << endl;
    cout << "Salir del programa _________ [0]" << endl;
    cout << "================================" << endl;
    cout << "Opción: ";

    cin >> opt;

    clear();

    return opt;
}


/* Función para el preguntar dato al usuario */
string solicitar_dato (string dato, string enunciado) {
    cout << "======== " << enunciado << " ========" << endl;
    cout << "Ingresar: ";
    cin >> dato;

    clear();

    return dato;
}


/* Función main */
int main(){
    string option = "\0";
    string dato = "\0";

    arbol_balance *arbol = new arbol_balance();
    Nodo *arbol_avl = NULL;

    while (option != "0") {

        option = menu_principal(option);

        if (option == "1") {
            dato = solicitar_dato(dato, "Proteína nueva");
            arbol_avl = arbol->insert_nodo(arbol_avl,dato);
            arbol->crear_grafo(arbol_avl);
        
        }else if (option == "2") {
            dato = solicitar_dato(dato, "Buscar proteína");
            arbol_avl = arbol->search_nodo(arbol_avl, dato);
        
        }else if (option == "3") {
            dato = solicitar_dato(dato, "Cambiar proteína");
            arbol_avl = arbol->change_nodo(arbol_avl, dato);
            arbol->crear_grafo(arbol_avl);
        
        }else if (option == "4") {
            dato = solicitar_dato(dato, "Eliminar proteína");
            arbol_avl = arbol->delete_nodo(arbol_avl, dato);
            arbol->crear_grafo(arbol_avl);
        
        }else if (option == "5") {
            arbol->mostrar_arbol(arbol_avl);

        }else if (option == "6") {
            dato = solicitar_dato(dato, "Nombre del archivo.txt");
            arbol_avl = arbol->lectura_txt(arbol_avl, dato);
            
        
        }else if (option == "7") {
            arbol->crear_grafo(arbol_avl);
        }
    }

    return 0;
}